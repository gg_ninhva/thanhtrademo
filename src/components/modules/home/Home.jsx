import React from 'react';
import style from "@blueprintjs/core";
import {DateInput} from "@blueprintjs/datetime";

class Home extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      date1: null,
      date2: null,
      date3: null,
      date4: null
    };

    this.data = [
      {
        id: '001',
        createDate: ' 09/03/2017',
        name: 'Nguyễn Công Minh',
        cardID: '201415669',
        address: '10 Nguyễn Như Hạnh - Đà Nẵng',
        content: 'Tố cáo hành vi lừa đảo chiếm đoạt tài sản',
        type: 'Tố cáo',
        peopleNumber: 1,
        expire: 'Còn 8 ngày',
        company: '',
        status: 'Chờ xử lý'
      },
      {
        id: '002',
        createDate: ' 09/03/2017',
        name: 'Đinh Văn Mạnh',
        cardID: '201415564',
        address: '100 Nguyễn Công Trứ - Đà Nẵng',
        content: 'Kiến nghị lấn chiếm vỉa hè làm nơi buôn bán',
        type: 'Kiến nghị phản ánh',
        peopleNumber: 3,
        expire: 'Còn 8 ngày',
        company: '',
        status: 'Chờ xử lý'
      },
      {
        id: '003',
        createDate: ' 09/03/2017',
        name: 'Lê Văn Tín',
        cardID: '201415675',
        address: 'K78/78/03 Cao Thắng - Đà Nẵng',
        content: 'Phản ánh về việc ô nhiễm',
        type: 'Kiến nghị phản ánh',
        peopleNumber: 3,
        expire: 'Còn 8 ngày',
        company: 'UBND Quận Hải Châu',
        status: 'Chờ xử lý'
      },
      {
        id: '004',
        createDate: ' 09/03/2017',
        name: 'Phan Thị Thơ',
        cardID: '201415997',
        address: 'K40/03 Thái Phiên - Đà Nẵng',
        content: 'Tranh chấp đất đai với bà Trần Thị Na',
        type: 'Tranh chấp đất',
        peopleNumber: 1,
        expire: 'Còn 8 ngày',
        company: '',
        status: 'Chờ xử lý'
      },
    ]
  }

  handleDateChange(name, value) {
    this.setState({
      [name]: new Date(value)
    });
  }

  render() {
    let list = this.data.map((item, index) => {
      return (
        <tr key={item.id}>
          <td>{index + 1}</td>
          <td>{item.id}</td>
          <td>{item.createDate}</td>
          <td>{item.name}</td>
          <td>{item.cardID}</td>
          <td>{item.address}</td>
          <td><a href="#">{item.content}</a></td>
          <td>{item.type}</td>
          <td>{item.peopleNumber}</td>
          <td>{item.expire}</td>
          <td>{item.company}</td>
          <td>{item.status}</td>
          <td>
            <button type="button" className="pt-button pt-icon-edit"></button>
            <button type="button" className="pt-button pt-icon-trash"></button>
          </td>
        </tr>
      );
    })
    return (
      <div className="main-container">
        <div className="container-fluid">
          <h3 className="title-block">Sơ đồ tiếp công dân chờ xử lý</h3>
          <div className="search-form">
            <div className="row">
              <div className="col-sm-8">
              </div>
              <div className="col-sm-4">
                <div className="row">
                  <div className="col-sm-6">
                    <button type="button" className="pt-button pt-icon-document pull-right">Xuất file</button>
                  </div>
                  <div className="col-sm-6">
                    <div className="pt-select pt-fill pull-right">
                      <select>
                        <option>Thêm mới</option>
                        <option value="1">Tiếp công dân thường xuyên</option>
                        <option value="2">Tiếp công dân định kỳ của lãnh đạo</option>
                        <option value="3">Tiếp công dân đột xuất của lãnh đạo</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <p>&nbsp;</p>
            </div>
            <div className="row">
              <div className="col-sm-2">
                <label className="pt-label">
                  Mã đơn:
                  <input className="pt-input pt-fill" type="text" placeholder="Text input" dir="auto" />
                </label>
              </div>
              <div className="col-sm-2">
                <label className="pt-label">
                  Tên người đứng đơn:
                  <input className="pt-input pt-fill" type="text" placeholder="Text input" dir="auto" />
                </label>
              </div>
              <div className="col-sm-2">
                <label className="pt-label">
                  CMND/Hộ chiếu:
                  <input className="pt-input pt-fill" type="text" placeholder="Text input" dir="auto" />
                </label>
              </div>
              <div className="col-sm-2">
                <label className="pt-label">
                  Phân loại đơn:
                  <div className="pt-select pt-fill">
                    <select>
                      <option>Choose an item...</option>
                      <option value="1">One</option>
                      <option value="2">Two</option>
                      <option value="3">Three</option>
                      <option value="4">Four</option>
                    </select>
                  </div>
                </label>
              </div>
              <div className="col-sm-2">
                <label className="pt-label pt-fill">
                  Phân loại tiếp công dân:
                  <div className="pt-select">
                    <select>
                      <option>Choose an item...</option>
                      <option value="1">One</option>
                      <option value="2">Two</option>
                      <option value="3">Three</option>
                      <option value="4">Four</option>
                    </select>
                  </div>
                </label>
              </div>
            </div>
            <div className="row">
              <div className="col-sm-2">
                <label className="pt-label pt-fill">
                  Tiếp nhận từ ngày:
                  <DateInput value={this.state.date} onChange={this.handleDateChange.bind('date1', this)} className="pt-fill" format="DD/MM/YYYY" closeOnSelection={false} />
                </label>
              </div>
              <div className="col-sm-2">
                <label className="pt-label pt-fill">
                  Đến ngày:
                  <DateInput value={this.state.date} onChange={this.handleDateChange.bind('date2', this)} className="pt-fill" format="DD/MM/YYYY" closeOnSelection={false} />
                </label>
              </div>
              <div className="col-sm-2">
                <label className="pt-label pt-fill">
                  Hạn giải quyết từ ngày:
                  <DateInput value={this.state.date} onChange={this.handleDateChange.bind('date3', this)} className="pt-fill" format="DD/MM/YYYY" closeOnSelection={false} />
                </label>
              </div>
              <div className="col-sm-2">
                <label className="pt-label pt-fill">
                  Đến ngày:
                  <DateInput value={this.state.date} onChange={this.handleDateChange.bind('date4', this)} className="pt-fill" format="DD/MM/YYYY" closeOnSelection={false} />
                </label>
              </div>
              <div className="col-sm-2">
                <label className="pt-label pt-fill pt-not-margin">&nbsp;</label>
                <button type="button" className="pt-button pt-icon-search pt-button-search">Tìm kiếm</button>
              </div>
            </div>
          </div>
          <div className="data-form">
            <div className="row">
              <div className="col-xs-12">
                <table className="pt-table data-table">
                  <thead>
                    <th>STT</th>
                    <th>Mã đơn</th>
                    <th>Ngày tiếp nhận</th>
                    <th>Tên người đứng đơn</th>
                    <th>CMND/Hộ chiếu</th>
                    <th>Địa chỉ</th>
                    <th>Nội dung vụ việc</th>
                    <th>Phân loại đơn</th>
                    <th>Số người</th>
                    <th>Hạn xử lý</th>
                    <th>Cơ quan giải quyết</th>
                    <th>Tình trạng xử lý</th>
                    <th width="90">Thao tác</th>
                  </thead>
                  <tbody>
                    {list}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Home;